### Setup

##### Prerequisites

Install [polymer-cli](https://github.com/Polymer/polymer-cli):

    npm install -g polymer-cli

### Start the development server from the working directory

This command serves the app at `http://localhost:8080` and provides basic URL
routing for the app:

    polymer serve
